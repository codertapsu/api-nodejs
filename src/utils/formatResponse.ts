import { Response } from 'express';
import * as HTTP_STATUS_CODE from 'http-status-codes';
const formatResponse = (res: Response, statusCode: number, data?: any, msg?: string) => {
  const message = msg || HTTP_STATUS_CODE.getStatusText(statusCode);
  return res.status(statusCode).send({
    data,
    message,
    time: Date.now()
  });
};
export default formatResponse;
