import fs from 'fs-extra';
import path from 'path';
import readline from 'readline';
import { google } from 'googleapis';
import { OAuth2Client, OAuth2ClientOptions } from 'google-auth-library/build/src/auth/oauth2client';

const SCOPES = [
  'https://www.googleapis.com/auth/gmail.send',
  'https://www.googleapis.com/auth/gmail.labels'
];
const TOKEN_PATH = '../config/token.json';

export class AuthorizeGoogle {
  private _oAuthOptions: OAuth2ClientOptions = {
    clientId: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    redirectUri: process.env.GOOGLE_REDIRECT_URL
  };
  private _oAuthClient: OAuth2Client;
  execute = async () => {
    this._oAuthClient = new google.auth.OAuth2(this._oAuthOptions);
    fs.readFile(path.resolve(__dirname, TOKEN_PATH), (err, token) => {
      if (err) {
        return this._getNewToken(this._oAuthClient);
      }
      this._oAuthClient.setCredentials(JSON.parse(token.toString()));
    });
  }
  getToken = async () => {
    const token = await fs.readFile(path.resolve(__dirname, TOKEN_PATH));
    return token.toString();
  }
  private _getNewToken(oAuth2Client: OAuth2Client) {
    oAuth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: SCOPES
    });
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    });
    rl.question('Enter the code from that page here: ', async (code: string) => {
      rl.close();
      const { tokens } = await oAuth2Client.getToken(code);
      oAuth2Client.setCredentials(tokens);
      await fs.writeFile(path.resolve(__dirname, TOKEN_PATH), JSON.stringify(tokens));
    });
  }
}
