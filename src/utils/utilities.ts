import bcrypt from 'bcrypt';
import uuidV5 from 'uuid/v5';
import jsonwebtoken, { SignOptions } from 'jsonwebtoken';
import { ObjectSchema } from '@hapi/joi';
import Config from '../config';

const BASE_UID = process.env.BASE_UID;
const JWT_SECRET_KEY = process.env.JWT_SECRET_KEY;

const encodeText = async (plainText: string) => {
    return bcrypt.hash(plainText, Config.saltRounds);
};

const generateUuid = async (name: string) => {
    return uuidV5(name, String(BASE_UID));
};

const generateToken = async (payload: string | Buffer | object) => {
    const option: SignOptions = { expiresIn: '1d', algorithm: 'HS512' };
    return jsonwebtoken.sign(payload, String(JWT_SECRET_KEY), option);
};
const verifyToken = async (token: string) => jsonwebtoken.verify(token, String(JWT_SECRET_KEY));

const validateModel = async (model: ObjectSchema, inputData: any): Promise<any> => {
    const resultValidate = model.validate(inputData);
    return new Promise((res, rej) => {
        if (resultValidate.error) {
            const error = resultValidate.error.details.reduce((result, errItem, index) => {
                result[index] = errItem.message;
                return result;
            }, new Object());
            rej(error);
        }
        res(resultValidate.value);
    });
};

export { encodeText, generateUuid, generateToken, verifyToken, validateModel };
