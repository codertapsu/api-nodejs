import fs from 'fs-extra';
const readHtmlFile = async (pathFile: string): Promise<any> => {
  return fs.readFile(pathFile, { encoding: 'utf-8' });
};
export default readHtmlFile;
