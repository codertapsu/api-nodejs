export interface IMediaInfo {
  readonly id?: any;
  filename: string;
  contentType: string;
  createAt: Date | number;
  path: string;
  image?: any;
}
