import UserController from '../controller/user.controller';
import { Router } from 'express';
import { USER_ROUTE } from './routes.constant';

const userController = new UserController();

const UserRoutes = (router: Router) => {
  router.route(USER_ROUTE.FILTER_USER).get(userController.findAll);
  router.route(USER_ROUTE.FIND_USER_BY_ID).get(userController.findById);
  router.route(USER_ROUTE.FIND_USER_BY_GUID).get(userController.findByGuId);
  router.route(USER_ROUTE.CHECK_EMAIL).get(userController.checkExistEmail);
};

export default UserRoutes;
