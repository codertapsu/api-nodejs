import { Router } from 'express';
import { START_ROUTE } from './routes.constant';
import StartController from '../controller/start.controller';

const startController = new StartController();

const StartRoutes = (router: Router) => {
    router.route(START_ROUTE.SIGN_IN).post(startController.signIn);
    router.route(START_ROUTE.SIGN_UP).post(startController.signUp);
};

export default StartRoutes;
