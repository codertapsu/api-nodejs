import MediaController from '../controller/media.controller';
import HandleFormData from '../middleware/handleFormData.mw';
import { Router } from 'express';
import FirebaseAdmin from '../middleware/firebase-auth.mw';

const handleFormData = new HandleFormData();
const mediaController = new MediaController();
const firebaseAdmin = new FirebaseAdmin();

const MediaRoutes = (router: Router) => {
  // Images
  router
    .route('/media/images')
    .get(mediaController.getAllImages)
    .post(firebaseAdmin.verifyToken, handleFormData.uploadFile('images').single('image'), mediaController.uploadOneImage)
    .delete(handleFormData.getSingleField(), mediaController.deleteImages);

  router
    .route('/media/image/:id')
    .get(mediaController.getImageById)
    .delete(mediaController.deleteImageById);

  router.route('/media/image/download/:id').get(mediaController.downloadImageById);

  router.route('/media/resize').post(handleFormData.readForm().single('image'), mediaController.uploadResizeImage);

  // Videos

  // Files
  router.route('/media/file')
    .post((req: any, res: any, next: any) => handleFormData
      .handleErr(req, res, next, handleFormData.uploadFile('files').array('file', 20)), mediaController.uploadMultiFiles);
  router.route('/media/file/download').get(handleFormData.getSingleField(), mediaController.downloadZipFiles);
};

export default MediaRoutes;
