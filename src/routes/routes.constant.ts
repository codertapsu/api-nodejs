const USER_ROUTE = {
    CHECK_EMAIL: '/user/check-email/:email',
    FILTER_USER: '/users',
    FIND_USER_BY_ID: '/users/:id',
    FIND_USER_BY_GUID: '/users/:guid',

};
const START_ROUTE = {
    SIGN_IN: '/sign-in',
    SIGN_UP: '/sign-up'
};

export {
    USER_ROUTE,
    START_ROUTE
};
