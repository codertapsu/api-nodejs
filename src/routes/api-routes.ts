import { Router } from 'express';
import UserRoutes from '../routes/user.routes';
import MediaRoutes from '../routes/media.routes';
import StartRoutes from '../routes/start.routes';

const router = Router();
router.get('/', (_, res) => {
    res.json({
        status: 'API Its Working',
        message: 'Welcome to Coder Tap Su Server!',
    });
});
UserRoutes(router);
MediaRoutes(router);
StartRoutes(router);

export default router;
