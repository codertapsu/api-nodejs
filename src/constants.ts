const COLLECTION_DB = {
    EMAILS: 'emails',
    USERS: 'users',
    IMAGES: 'images',
    FILES: 'files'
};

export {
    COLLECTION_DB
};
