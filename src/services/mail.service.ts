import path from 'path';
import { AuthenticationTypeOAuth2 } from 'nodemailer/lib/smtp-connection';
import { SendMailOptions, createTransport } from 'nodemailer';
import { compile } from 'handlebars';
import readHtmlFile from '../utils/readHtmlFile';
import { AuthorizeGoogle } from '../utils/authorize.google';

export default class MailService {
  private _authorizeGoogle: AuthorizeGoogle = new AuthorizeGoogle();
  private _auth: AuthenticationTypeOAuth2 = {
    type: 'OAuth2',
    user: 'hoangduykhanh21@gmail.com',
    accessToken: ''
  };
  private _smtpTransport = createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: this._auth
  });
  private _mailOptions: SendMailOptions = {
    from: '"Test function" <hoangduykhanh.dn@gmail.com>',
    to: 'hoangduykhanh.dn@gmail.com',
    subject: 'Subject test await'
  };
  sendEmail = async (templateName?: string) => {
    try {
      await this._authorizeGoogle.execute();
      const tokenGoogle = await this._authorizeGoogle.getToken();
      this._auth.accessToken = tokenGoogle['access_token'];
      const result = await readHtmlFile(path.resolve(__dirname, `../email-templates/${templateName || 'travel.html'}`));
      const template = compile(result);
      const replacements = {
        username: 'John Doe'
      };
      const htmlToSend = template(replacements);
      const resultSentEmail = await this._smtpTransport.sendMail({ ...this._mailOptions, ...{ html: htmlToSend } });
      return resultSentEmail;
    } catch (error) {
      throw error;
    }
  }
}
