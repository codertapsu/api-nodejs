import { Collection, FindOneOptions, FilterQuery } from 'mongodb';
import { getDb, getPrimaryKey } from '../db/connectDb';
import { COLLECTION_DB } from '../constants';
import IUserInfo from '../interfaces/userInfo.interface';

export default class UserService {
    private _usersCollection: Collection<IUserInfo>;
    private _emailsCollection: Collection<any>;

    constructor() {
        this._usersCollection = getDb().collection<IUserInfo>(COLLECTION_DB.USERS);
        this._emailsCollection = getDb().collection<any>(COLLECTION_DB.EMAILS);
    }

    findUserByEmail = async (email: string) => {
        return this._usersCollection.findOne({ email });
    }
    findUserByUserName = async (userName: string) => {
        return this._usersCollection.find({ userName });
    }
    findExistEmail = async (email: string) => {
        return this._emailsCollection.findOne({ email });
    }
    insertUser = async (userInfo: IUserInfo) => {
        return this._usersCollection.insertOne(userInfo);
    }

    findUserById = async (id: any) => {
        const option: FindOneOptions = {
            fields: {
                _id: 0,
                password: 0,
                access_token: 0
            }
        };
        return this._usersCollection.findOne({ _id: getPrimaryKey(id) }, option);
    }

    findUserByGuid = async (guid: string) => {
        const option: FindOneOptions = {
            fields: {
                _id: 0,
                password: 0,
                access_token: 0
            }
        };
        return this._usersCollection.findOne({ guid }, option);
    }

    filterUsers = async (search: string) => {
        const option: FindOneOptions = {
            fields: {
                _id: 0,
                password: 0,
                access_token: 0
            }
        };
        const query: FilterQuery<IUserInfo> = {
            $or: [
                {
                    email: { $regex: new RegExp(search) }
                },
                {
                    userName: { $regex: new RegExp(search) }
                }
            ]
        };
        return this._usersCollection.find(query, option).sort({ createAt: 1 }).toArray();
    }
    updateOneUser = async (email: string, accessToken: string) => {
        return this._usersCollection.updateOne({ email }, {
            $set: {
                accessToken
            }
        });
    }
}
