import { Collection } from 'mongodb';
import { getDb, getPrimaryKey } from '../../db/connectDb';
import formatResponse from '../../utils/formatResponse';
import { Response, Request } from 'express';
import fs from 'fs-extra';
import { IMediaInfo } from '../../interfaces/media.interface';

export default class ImageService {
    regexCutExt = /\.[^.]{0,4}$/i;
    imagesCollection = (): Collection<IMediaInfo> => getDb().collection('images');

    getImages = async (req: Request, res: Response) => {
        const results = await this.imagesCollection()
            .find(
                {},
                {
                    fields: {
                        contentType: 0
                    }
                }
            )
            .sort({ createAt: 1 })
            .toArray();
        if (!results.length) {
            return formatResponse(res, 200, [], 'Empty Results.');
        }
        formatResponse(res, 200, results);
    }

    deleteImagesIds = async (req: Request, res: Response) => {
        const formData = req.body;
        if (!{ ...formData }.hasOwnProperty('Ids') || Number(Object.keys({ ...formData }).length) !== 1) {
            const message = `Format Error: FormData with key Ids`;
            return formatResponse(res, 400, null, message);
        }
        let ids: Number[] = [];
        (typeof formData['Ids'] === 'string') ? ids.push(Number(formData['Ids'])) : (ids = [...formData['Ids']].map(x => Number(x)));
        const result = await this.imagesCollection().deleteMany({ _id: { $in: ids.map((id: any) => getPrimaryKey(id)) } });
        formatResponse(res, 200, null, (result && result.deletedCount) ? `Deleted ${result.deletedCount} images` : 'Deleted');
    }

    uploadOneImage = async (req: Request, res: Response) => {
        if (!req.file) {
            return formatResponse(res, 400, null, 'No file.');
        }
        // const image = fs.readFileSync(req.file.path);
        // const encode_image = image.toString('base64');
        const finalImg = {
            filename: req.file.originalname.replace(this.regexCutExt, '').replace(/\s/gm, '_'),
            contentType: req.file.mimetype,
            createAt: Date.now(),
            // image: Buffer.from(encode_image, 'base64'),
            path: req.file.path
        };
        const result = await this.imagesCollection().insertOne(finalImg);
        const {
            contentType,
            image,
            ...responseResult
        } = result.ops[0];
        formatResponse(res, 200, responseResult);
    }
    getImageById = async (req: Request, res: Response) => {
        const id = req.params.id;
        const result = await this.imagesCollection().findOne({ _id: getPrimaryKey(id) });
        if (!result) {
            return formatResponse(res, 404, null, 'Image Not Found.');
        }
        const {
            contentType,
            image,
            ...responseResult
        } = result;
        formatResponse(res, 200, responseResult);
    }
    downloadImageById = async (req: Request, res: Response) => {
        const id = req.params.id;
        const result = await this.imagesCollection().findOne({ _id: getPrimaryKey(id) });
        if (!result) {
            return formatResponse(res, 404, null, 'Image Not Found.');
        }
        // const filename = await `${result.filename}.${result.contentType.match(/([^\/]*)$/i)[0]}`;
        // res.contentType(result.contentType);
        // res.attachment(filename);
        // res.status(200).send(result.image.buffer);
        formatResponse(res, 200, result.path);
    }
    deleteImageById = async (req: Request, res: Response) => {
        const id = req.params.id;
        const image = await this.imagesCollection().findOne({ _id: getPrimaryKey(id) });
        await fs.remove(image!.path);
        await this.imagesCollection().findOneAndDelete({ _id: getPrimaryKey(id) });
        formatResponse(res, 200, null, 'Deleted.');
    }
}
