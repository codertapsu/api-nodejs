import { Collection } from 'mongodb';
import { getDb } from '../db/connectDb';
import IUserInfo from '../interfaces/userInfo.interface';

export default class LoginService {
    usersCollection = (): Collection<IUserInfo> => getDb().collection('users');

    login = async (body: any) => {
        return this.usersCollection().findOne({
            $or: [{
                userName: body.userName
            }, {
                email: body.email
            }]
        });

    }
}
