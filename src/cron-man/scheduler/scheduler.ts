const scheduler = (timer: number, action: any) => {
  setInterval(action, timer);
};

process.nextTick(() => scheduler);

export default scheduler;
