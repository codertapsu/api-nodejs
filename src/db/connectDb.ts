import { MongoClient, ObjectID, Db, MongoClientOptions } from 'mongodb';
const dbName = process.env.DB_NAME || 'learning';
const dbHost = process.env.DB_HOST || 'mongodb://127.0.0.1:27017';
const mongoOptions: MongoClientOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true
};
let dataBase: Db;
const createConnectionDb = async () => {
    try {
        const dbClient = await MongoClient.connect(dbHost, mongoOptions);
        dataBase = dbClient.db(dbName);
        return Promise.resolve(1)
    } catch (error) {
        return Promise.resolve(0);
    }
};
const getPrimaryKey = (_id: string | number | ObjectID) => new ObjectID(_id);
const getDb = (): Db => dataBase;

export { createConnectionDb, getPrimaryKey, getDb };
