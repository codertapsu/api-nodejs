import { Request, Response, NextFunction } from 'express';
import bcrypt from 'bcrypt';
import * as HTTP_STATUS_CODE from 'http-status-codes';
import { encodeText, generateUuid, generateToken, validateModel } from '../utils/utilities';
import formatResponse from '../utils/formatResponse';
import { UserModel } from '../model/user.model';
import { IResponseError } from '../model/response-error.interface';
import IUserInfo from '../interfaces/userInfo.interface';
import LoginService from '../services/login.service';
import UserService from '../services/user.service';
import MailService from '../services/mail.service';

export default class StartController {
  private _loginService = new LoginService();
  private _userService = new UserService();
  private _mailService = new MailService();

  signIn = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const body = req.body;
      const user = await this._loginService.login(body);
      if (!user) {
        return formatResponse(res, HTTP_STATUS_CODE.NOT_FOUND);
      }
      const matchPassword = await bcrypt.compare(body.password, user!.password);
      if (!matchPassword) {
        return formatResponse(res, HTTP_STATUS_CODE.UNAUTHORIZED);
      }
      if (matchPassword) {
        const message = `Welcome! ${user.userName}`;
        const accessToken = await generateToken({ email: user.email, userName: user.userName });
        await this._userService.updateOneUser(user.email, accessToken);
        return formatResponse(res, HTTP_STATUS_CODE.OK, { token: accessToken }, message);
      }
      return formatResponse(res, HTTP_STATUS_CODE.UNAUTHORIZED);
    } catch (error) {
      const err: IResponseError = {
        success: false,
        code: HTTP_STATUS_CODE.SERVICE_UNAVAILABLE,
        error
      };
      next(err);
    }
  }

  signUp = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const userInfo = req.body;
      const validateInfo = await validateModel(UserModel, userInfo);
      const hashPass = await encodeText(validateInfo.password);
      const guid = await generateUuid(validateInfo.email);
      const saveUserInfo: IUserInfo = {
        ...validateInfo,
        password: hashPass,
        guid
      };
      const result = await this._userService.insertUser(saveUserInfo);
      const { password, _id, ...responseData } = result.ops[0];
      return formatResponse(res, HTTP_STATUS_CODE.OK, responseData);
    } catch (error) {
      return formatResponse(res, HTTP_STATUS_CODE.BAD_REQUEST, error, error.message);
    }
  }

  sendEmail = async (req: Request, res: Response) => {
    try {
      const result = await this._mailService.sendEmail();
      formatResponse(res, 200, null, String(result));
    } catch (error) {
      formatResponse(res, 500, null, error && error.message);
    }
  }
}
