import { Request, Response, NextFunction } from 'express';
import * as HTTP_STATUS_CODE from 'http-status-codes';
import formatResponse from '../utils/formatResponse';
import { IResponseError } from '../model/response-error.interface';
import UserService from '../services/user.service';

export default class UserController {
    private _userService = new UserService();
    checkExistEmail = async (req: Request, res: Response, next: NextFunction) => {
        const email: string = req.params.email;
        if (!email) {
            const err: IResponseError = {
                success: false,
                code: HTTP_STATUS_CODE.BAD_REQUEST,
            };
            return next(err);
        }
        try {
            const user = await this._userService.findUserByEmail(email);
            const isExisted = !!user ? true : false;
            return formatResponse(res, HTTP_STATUS_CODE.OK, { isExisted });
        } catch (error) {
            const err: IResponseError = {
                success: false,
                code: HTTP_STATUS_CODE.BAD_REQUEST,
                error
            };
            next(err);
        }
    }

    findAll = async (req: Request, res: Response, next: NextFunction) => {
        const { search } = req.query;
        try {
            const results = await this._userService.filterUsers(search);
            return formatResponse(res, HTTP_STATUS_CODE.OK, results);
        } catch (error) {
            const err: IResponseError = {
                success: false,
                code: HTTP_STATUS_CODE.BAD_REQUEST,
                error
            };
            next(err);
        }
    }

    findById = async (req: Request, res: Response, next: NextFunction) => {
        const id = req.query && req.query.id || req.params && req.params.id || req.body && req.body.id;
        if (!id) {
            const err: IResponseError = {
                success: false,
                code: HTTP_STATUS_CODE.BAD_REQUEST,
            };
            return next(err);
        }
        try {
            const user = await this._userService.findUserById(id);
            return formatResponse(res, HTTP_STATUS_CODE.OK, user);
        } catch (error) {
            const err: IResponseError = {
                success: false,
                code: HTTP_STATUS_CODE.BAD_REQUEST,
                error
            };
            next(err);
        }
    }
    findByGuId = async (req: Request, res: Response, next: NextFunction) => {
        const guid = req.query && req.query.guid || req.params && req.params.guid || req.body && req.body.guid;
        if (!guid) {
            const err: IResponseError = {
                success: false,
                code: HTTP_STATUS_CODE.BAD_REQUEST,
            };
            return next(err);
        }
        try {
            const user = await this._userService.findUserByGuid(guid);
            return formatResponse(res, HTTP_STATUS_CODE.OK, user);
        } catch (error) {
            const err: IResponseError = {
                success: false,
                code: HTTP_STATUS_CODE.BAD_REQUEST,
                error
            };
            next(err);
        }
    }
}
