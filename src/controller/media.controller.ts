import path from 'path';
import formatResponse from '../utils/formatResponse';
import fs from 'fs-extra';
import { getDb, getPrimaryKey } from '../db/connectDb';
import archiver from 'archiver';
import ImageService from '../services/media_services/image.service';
import ResizeImage from '../middleware/resizeImage.mw';
import { Response, Request, NextFunction } from 'express';

// const IMAGES_COLLECTION = 'images';
const FILES_COLLECTION = 'files';

const regexCutExt = /\.[^.]{0,4}$/i;

export default class MediaController {
  private _imageService: ImageService;
  private _resizeImage: ResizeImage;
  constructor() {
    this._imageService = new ImageService();
    this._resizeImage = new ResizeImage();
  }

  getAllImages = async (req: Request, res: Response) => {
    try {
      this._imageService.getImages(req, res);
    } catch (error) {
      formatResponse(res, 500, null, error && error.message);
    }
  }

  deleteImages = async (req: Request, res: Response) => {
    try {
      this._imageService.deleteImagesIds(req, res);
    } catch (error) {
      formatResponse(res, 500, null, error && error.message);
    }
  }

  uploadOneImage = async (req: Request, res: Response) => {
    try {
      this._imageService.uploadOneImage(req, res);
    } catch (error) {
      formatResponse(res, 500, null, error && error.message);
    }
  }

  uploadResizeImage = async (req: Request, res: Response) => {
    try {
      if (!req.file) {
        return formatResponse(res, 400, null, 'No file provided');
      }
      const fileUrl = await this._resizeImage.save(req.file.buffer);
      return formatResponse(res, 200, { thumbnail: fileUrl });
    } catch (error) {
      formatResponse(res, 500, null, error && error.message);
    }
  }

  getImageById = async (req: Request, res: Response) => {
    try {
      this._imageService.getImageById(req, res);
    } catch (error) {
      formatResponse(res, 500, null, error && error.message);
    }
  }

  deleteImageById = async (req: Request, res: Response) => {
    try {
      this._imageService.deleteImageById(req, res);
    } catch (error) {
      formatResponse(res, 500, null, error && error.message);
    }
  }

  downloadImageById = async (req: Request, res: Response) => {
    try {
      this._imageService.downloadImageById(req, res);
    } catch (error) {
      formatResponse(res, 500, null, error && error.message);
    }
  }

  uploadMultiFiles = async (req: Request, res: Response, _: NextFunction) => {
    try {
      const files = req.files as any[];
      const finalFiles = files.map(fileItem => ({
        filename: fileItem.originalname.replace(regexCutExt, '').replace(/\s/gm, '_'),
        contentType: fileItem.mimetype,
        createAt: Date.now(),
        path: fileItem.path
      }));
      return getDb().collection(FILES_COLLECTION).insertMany(finalFiles, (err, result) => {
        if (err) {
          formatResponse(res, 500, null, err && err.message);
        }
        const response = result.ops.map(x => ({
          filename: x.filename,
          createAt: x.createAt,
          path: x.path,
          _id: x._id
        }));
        const message = `Upload Success!`;
        formatResponse(res, 200, response, message);
      });
    } catch (error) {
      formatResponse(res, 500, null, error && error.message);
    }
  }

  downloadZipFiles = async (req: Request, res: Response, _: NextFunction) => {
    try {
      const formData = req.body;
      if (
        !{
          ...formData
        }.hasOwnProperty('Ids') ||
        Number(
          Object.keys({
            ...formData
          }).length
        ) !== 1
      ) {
        const message = `Format Error: FormData with key Ids`;
        formatResponse(res, 400, null, message);
      }
      return getDb().collection(FILES_COLLECTION)
        .find({
          _id: {
            $in: formData.Ids.map((id: any) => getPrimaryKey(id))
          }
        })
        .toArray(async (err, data) => {
          if (err) {
            formatResponse(res, 500, null, err && err.message);
          }
          const output = fs.createWriteStream('./uploads/downloadFiles.zip');
          const archive = archiver('zip', {
            zlib: {
              level: 9
            }
          });
          archive.pipe(output);
          data.forEach(x => {
            const fileItem = path.resolve(__dirname, `../../${x.path}`);
            archive.append(fs.createReadStream(fileItem), {
              name: `${x.filename}${x.path.match(regexCutExt)[0]}`
            });
          });
          output.on('close', () => {
            res.status(200).download(String(output.path));
            // return next();
          });
          archive.finalize();
        });
    } catch (error) {
      formatResponse(res, 500, null, error && error.message);
    }
  }
}
