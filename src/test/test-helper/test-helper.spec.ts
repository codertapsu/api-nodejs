import 'mocha';
import express from 'express';
import chai from 'chai';
import chaiHttp from 'chai-http';
import { AppRoot } from '../../app';

chai.use(chaiHttp);
let appRoot: AppRoot;
export let app: express.Application;
export let authToken: string;

const loginInfo = {
    email: 'marchoang@gmail.com',
    password: '123456'
};

before(async () => {
    appRoot = await new AppRoot();
    await appRoot.setUpServer();
    app = appRoot.app;
    let response = await chai
        .request(app)
        .post('/api/sign-in')
        .send(loginInfo);
    authToken = response.body.token;
});
