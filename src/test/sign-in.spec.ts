import 'mocha';
import chai from 'chai';
import chaiHttp from 'chai-http';
import { app } from './test-helper/test-helper.spec';

const loginInfoSuccess = {
  email: 'marchoang@gmail.com',
  password: '123456'
};
const loginInfoNotExist = {
  email: 'nmarchoang@gmail.com',
  password: '123456'
};
const loginInfoWrongPassword = {
  email: 'marchoang@gmail.com',
  password: '1234562'
};
chai.use(chaiHttp);
const should = chai.should();

describe('Login', () => {
  describe('POST /api/sign-in', () => {
    it('should able to login', done => {
      chai
        .request(app)
        .post('/api/sign-in')
        .send(loginInfoSuccess)
        .end((err, res) => {
          should.not.exist(err);
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('data');
          res.body.should.have.property('time');
          //       res.body.should.have.property('success');
          done();
        });
    });
    it('should not found email', done => {
      chai
        .request(app)
        .post('/api/sign-in')
        .send(loginInfoNotExist)
        .end((err, res) => {
          should.not.exist(err);
          res.should.have.status(404);
          res.body.should.be.a('object');
          res.body.should.have.property('time');
          //       res.body.should.have.property('success');
          done();
        });
    });
    it('should error password', done => {
      chai
        .request(app)
        .post('/api/sign-in')
        .send(loginInfoWrongPassword)
        .end((err, res) => {
          should.not.exist(err);
          res.should.have.status(401);
          res.body.should.be.a('object');
          res.body.should.have.property('time');
          //       res.body.should.have.property('success');
          done();
        });
    });
  });
});
