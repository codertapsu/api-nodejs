const Config = {
    development: {
        host: process.env.HOST || '0.0.0.0',
        port: process.env.PORT || 8080,
    },
    production: {
        port: 9000,
    },
    saltRounds: 10
};
export default Config;
