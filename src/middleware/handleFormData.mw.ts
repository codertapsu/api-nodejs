import multer, { StorageEngine } from 'multer';
import fs from 'fs-extra';
import path from 'path';
import formatResponse from '../utils/formatResponse';

export default class HandleFormData {
  private _formData: multer.Instance;
  constructor() {
    this._formData = multer({
      limits: { fieldSize: 5 * 1024 * 1024 }
    });
  }
  readForm = () => this._formData;
  getSingleField = () => this._formData.fields([]);
  getArrayFields = (fieldName: string) => this._formData.array(fieldName);
  uploadFile = (folderName: string) => {
    return multer({
      storage: this._storage(folderName),
      limits: {
        fieldSize: 1024 * 1024 * 5
      }
    });
  }

  private _storage = (folderName: string): StorageEngine => {
    const isExistedFolder = fs.existsSync(path.resolve(__dirname, `../../uploads`)) && fs.existsSync(path.resolve(__dirname, `../../uploads/${folderName}`));
    if (!isExistedFolder) {
      fs.mkdirSync(path.resolve(__dirname, `../../uploads/${folderName}`), { recursive: true });
    }
    return multer.diskStorage({
      destination: (req, file, cb) => {
        cb(null, `uploads/${folderName}`);
      },
      filename: (req, file, cb) => {
        cb(null, Date.now() + '__' + file.originalname.replace(/\s/gm, '_'));
      }
    });
  }

  handleErr = (req, res, next, method) => {
    method(req, res, err => {
      // if (err instanceof multer.MulterError) {
      //     return res.status(400).send(formatResponse(err, 'Dto invalid'));
      // }
      if (err) {
        formatResponse(res, 400, { ...err }, 'Invalid request');
      } else {
        return next();
      }
    });
  }
}
