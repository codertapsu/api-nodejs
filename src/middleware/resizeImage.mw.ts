import sharp from 'sharp';
import uuid from 'uuid/v4';
import path from 'path';
import fs from 'fs-extra';

export default class ResizeImage {
  static filename = () => `${uuid()}.png`;
  save = async (buffer: Buffer) => {
    const filename = ResizeImage.filename();
    const { filePath, fileUrl } = await this._storage(filename);
    await sharp(buffer)
      .resize(300, null, {
        fit: sharp.fit.inside,
        withoutEnlargement: true
      })
      .toFile(filePath);
    return fileUrl;
  }

  private _storage = async (filename: string): Promise<any> => {
    const pathFolder = await path.join(__dirname, `../../uploads/resize`);
    const isExistedFolder = await fs.existsSync(pathFolder);
    if (!isExistedFolder) {
      await fs.mkdirSync(pathFolder, { recursive: true });
    }
    return {
      filePath: path.resolve(`${pathFolder}/${filename}`),
      fileUrl: `uploads/resize/${filename}`
    };
  }
}
