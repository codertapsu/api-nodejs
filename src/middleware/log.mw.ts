import fs, { WriteStream } from 'fs-extra';
import path from 'path';
import morgan from 'morgan';

const createLogFile = (): WriteStream => {
  const isExistedFile = fs.existsSync(path.resolve(__dirname, `../../logger`)) && fs.existsSync(path.resolve(__dirname, `../../logger/access.log`));
  if (!isExistedFile) {
    fs.mkdirSync(path.resolve(__dirname, `../../logger`), { recursive: true });
  }
  return fs.createWriteStream(path.join(__dirname, '../../logger/access.log'), { flags: 'a' });
};
export const logger = morgan('short', { skip: () => process.env.NODE_ENV === 'development', stream: createLogFile() });
