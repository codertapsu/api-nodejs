import * as admin from 'firebase-admin';
import { Request, Response } from 'express';
import formatResponse from '../utils/formatResponse';

export default class FirebaseAdmin {
  private _serviceAccount: admin.ServiceAccount = {
    projectId: process.env.FIREBASE_PROJECT_ID,
    clientEmail: process.env.FIREBASE_CLIENT_EMAIL,
    privateKey: process!.env!.FIREBASE_PRIVATE_KEY!.replace(/\\n/g, '\n')
  };
  constructor() {
    admin.initializeApp({
      credential: admin.credential.cert(this._serviceAccount),
      databaseURL: process.env.FIREBASE_DATABASE_URL
    });
  }
  verifyToken = async (req: Request, res: Response, next: any) => {
    try {
      const token = req.headers['x-access-token'] || req.headers['authorization'] || req.query.token || req.body.token;
      if (!token) {
        const message = `Request is not contain Auth token`;
        return formatResponse(res, 403, null, message);
      }
      const decodedToken = await admin.auth().verifyIdToken(token);
      if (decodedToken) {
        req.body.uid = decodedToken.uid;
        return next();
      }
      return formatResponse(res, 401, null);
    } catch (error) {
      formatResponse(res, 500, null, error && error.message);
    }
  }
}
