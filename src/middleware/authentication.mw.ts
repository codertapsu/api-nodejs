import { Request, Response, NextFunction } from 'express';
import { verifyToken } from '../utils/utilities';
import formatResponse from '../utils/formatResponse';

export default class Authentication {
    checkAuth = async (req: Request, res: Response, next: NextFunction) => {
        try {
            let token = req.headers['x-access-token'] || req.headers['authorization'] || req.query.token || req.body.token;
            if (!token) {
                const message = `Request is not contain Auth token`;
                return formatResponse(res, 403, null, message);
            }
            if (token.startsWith('Bearer ')) {
                token = token.slice(7, token.length);
            }
            const resultVerify = await verifyToken(token);
            if (!resultVerify) {
                throw Error(`jwt not working.`);
            }
            next();
        } catch (error) {
            formatResponse(res, 500, null, error && error.message);
        }
    }
}
