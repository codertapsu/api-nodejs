import http from 'http';
import express, { Application } from 'express';
import cors from 'cors';
import WS, { Server } from 'ws';
import bodyParser from 'body-parser';
import swaggerUI from 'swagger-ui-express';
import 'dotenv/config';

import { logger } from './middleware/log.mw';
import Config from './config';
import * as swaggerDocument from './swagger/swagger.json';

import { createConnectionDb } from './db/connectDb';

// tslint:disable-next-line: no-var-requires
require('./cron-man/cron-man');

export class AppRoot {
  private _app: Application;
  private _wss: Server;
  get app() {
    return this._app;
  }
  constructor() {
    this._app = express();
    this._app.use(cors());
    this._app.use(logger);
    this._app.use(bodyParser.urlencoded({ extended: false }));
    this._app.use(bodyParser.json());
    this._app.use((_, res, next) => {
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
      res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
      next();
    });
    this._app.use('/uploads', express.static('uploads'));
    this._app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocument));
    this._app.get('/', (_, res) => {
      res.send({
        message: 'Welcome to API server CoderTapSu'
      });
    });
  }
  setUpServer = async () => {
    await this._startDb();
    await this._startServer();
  }
  private _startServer = async (): Promise<boolean> => {
    const server = http.createServer(this._app);
    const stage = Config.development;
    this._wss = new Server({ server });
    this._wss.on('connection', (ws: WS, _: http.IncomingMessage) => {
      ws.on('message', (message: string) => ws.send(message));
      ws.send(`Hi, WS is running!`);
    });
    return new Promise((res, _) => server.listen(Number(stage.port), stage.host, () => {
      res(true);
    }).on('on', this._nodeErrorHandler));
  }
  private _startDb = async () => {
    const codeDb = await createConnectionDb();
    if (!!codeDb) {
      const router = await import('./routes/api-routes');
      this._app.use('/api', router.default);
    }
  }

  private _nodeErrorHandler = (err: NodeJS.ErrnoException): void => {
    switch (err.code) {
      case 'EACCES':
        process.exit(1);
        break;
      default:
        throw err;
    }
  }
}
